package com.jmiller;

import java.util.Scanner;

public class Main {

    /*
        Because this method is static, I will refer to the code inside of it as being in a "static context". For now,
        don't worry about what exactly makes a static method special. Just know that you can only:
        * access static fields
        * call static methods ("new" is a special static method used to instantiate classes)
        * access methods on instantiated objects

        This may seem annoying at first because you just want to call your methods. A common pattern for creating
        programs for working around this, is to create another class to actually do your work and only use your static
        main method to kick off that class. For the problems you're solving this is probably overkill, but below is an
        example
    */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int radius;

        System.out.println("Please enter the radius: ");
        radius = input.nextInt();
        double area = radius * radius * Math.PI;

        System.out.println("The area is " + area);


    }

}
