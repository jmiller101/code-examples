package com.jmiller.accessors;

public class FieldAccess {
    public String stringValue;

    public int intValue;

    public FieldAccess(String stringValue, int intValue) {
        this.stringValue = stringValue;
        this.intValue = intValue;
    }
}
