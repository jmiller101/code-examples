package com.jmiller.accessors;

public class MethodAccess {
    private String stringValue;
    private int intValue;

    public MethodAccess(String stringValue, int intValue) {
        this.stringValue = stringValue;
        this.intValue = intValue;
    }

    public String getStringValue() {
        return stringValue;
    }

//    public String getStringValue() {
//        return stringValue.trim();
//    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

//    public void setStringValue(String stringValue) {
//        this.stringValue = stringValue.trim();
//    }

//    public void setStringValue(String stringValue) {
//        if (stringValue.contains(" ")) {
//            throw new RuntimeException();
//        }
//        this.stringValue = stringValue;
//    }

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }
}
