package com.jmiller;

import com.jmiller.accessors.FieldAccess;
import com.jmiller.accessors.MethodAccess;

/*
    Zooming out from the code a bit, Java has a lot of conventions. A convention is a common practice that many people
    use and they are convenient because it allows you to more easily understand other people's code when they follow
    common conventions. However, their use is not required. Also, if people break convention without calling it out
    explicitly, their code can get very confusing because things that you are expecting to happen... won't.

    Getters and Setters (aka "accessors") are an example of an extremely common convention in Java. In general, their
    use allows for greater flexibility when implementing your code. With a field, you cannot attach any logic when you
    get or set a value on a class. Exactly what you put after the "=" is what the field becomes.

    However, lets say you were writing a big program that uses your class a lot. And lets say that you want a certain
    String field to always have its whitespace stripped.
    There are a few ways of accomplishing this:
    * Throw an error when someone tries to set with a String that has whitespace
    * Strip the value yourself when it gets set
    * Strip the value yourself when someone calls the getter

    If you started your code by using a setter for that field, you can easily change your mind and just update the
    setter method for that field. Because the rest of your code just calls your setter already, there's not work that
    you need to do. However if you started with using fields and then you decided that you wanted logic around your get/
    set behaviour, now you need to go and update your code everywhere.

    Take a look in the MethodAccess class for examples. By just commenting and uncommenting the different getters and
    setters you can change the functionality of the program. If I had used fields instead, you would need to write or
    change a lot more code every time you wanted to try a different functionality. Also, in a large codebase which might
    read a field in literally 100s of places, having the ability to change code in one place and know that it will
    propagate across the codebase in invaluable.

    Keep in mind that at the end of the day they are conventions. They are just normal methods that can be changed or
    used in all the same ways that any other methods are.
 */
public class MainAccessors {

    public static void main(String[] args) {
        fieldExamples();
        methodExamples();
    }

    private static void fieldExamples() {
        FieldAccess fa = new FieldAccess("hello, world!", 5);

        System.out.println(fa.intValue);
        System.out.println(fa.stringValue);

        fa.intValue = 10;
        fa.stringValue = "changed";

        System.out.println(fa.intValue);
        System.out.println(fa.stringValue);
    }

    private static void methodExamples() {
        MethodAccess ma = new MethodAccess("hello, world!", 5);

        // These are both compiler errors because the fields of MethodAccess are private
        // System.out.println(ma.intValue);
        // System.out.println(ma.stringValue);

        System.out.println(ma.getIntValue());
        System.out.println(ma.getStringValue());

        ma.setIntValue(10);
        // I've put lots of whitespace before and after this string so you can see how the value changes
        ma.setStringValue("       changed        ");

        System.out.println(ma.getIntValue());
        // I've added the * characters to make it obvious whether whitespace has been stripped or not. Go change what
        // getter and setter are uncommented and see how this output changes
        System.out.println("*" + ma.getStringValue() + "*");
    }

}
