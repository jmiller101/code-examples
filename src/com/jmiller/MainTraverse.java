package com.jmiller;

import java.util.ArrayList;
import java.util.List;

public class MainTraverse {

    /*
        The main thing to notice here is that arrays are a primitive type. They do not have any methods on them, only
        a public field called "length". That means you can initialize them with "{}" and you access their items with
        "[]".

        Before you read the next part, make sure you know the basics about interfaces, inheritance, and generic types...
        It will make it much easier to understand. TL;DR: Because ArrayLists are objects and not primitives, you need to
        use methods to interact with them. You cannot use "[]" or "{}".

        https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html

        List is an interface and ArrayList is a class which implements the List interface. ArrayLists are objects and
        can only hold other objects! For example, you cannot have a List<int>; it needs to be a List<Integer>. The thing
        in the "<>" tells Java what type can be held in the List that you have created. So you could have a List<Object>
        or List<String> for example.

        Because an ArrayList implements that List interface, to access it you need to use the methods defined in the
        List interface. You cannot use "[]" like you do with raw arrays. However, you can still use for-each loops
        because Lists also implement the Iterable interface if you go up through its inheritance hierarchy.
     */
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};

        System.out.println("Array for loop");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        System.out.println("Array while loop");
        int j = 0;
        while (j < array.length) {
            System.out.println(array[j]);
            j++;
        }

        System.out.println("Array do-while loop");
        j = 0;
        do {
            System.out.println(array[j]);
            j++;
        } while (j < array.length);

        System.out.println("Array for-each loop");
        for (int i : array) {
            System.out.println(i);
        }

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        System.out.println("List for loop");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("List while loop");
        j = 0;
        while (j < list.size()) {
            System.out.println(list.get(j));
            j++;
        }

        System.out.println("List do-while loop");
        j = 0;
        do {
            System.out.println(list.get(j));
            j++;
        } while (j < list.size());

        System.out.println("List for-each loop");
        for (Integer i : list) {
            System.out.println(i);
        }
    }

}
