package com.jmiller;

import static java.lang.System.currentTimeMillis;

/*
	Alright, so I've taken the algorithms you submitted and put them both in this file in order to show you something
	neat! The code below might look complicated and a little cryptic... But I promise it's simple to understand. I took
	the methods you sent me and I printed out 2 things:
		1. The execution time of each method.
		2. The memory usage of the program at the beginning and end of each method.

	I've also wrapped them both in for loops so that it can print some extra data points.

	Some interesting things to look at:
		1. What is the difference in the first and second memory usage readings from each method? Can you explain where
			that extra memory usage comes from?
		2. The times generally get faster the longer the program runs. This is due to how the Java Virtual Machine (JVM)
			continues to optimize the CPU time of your code as it runs and learns.
		3. However, the memory usage does not go down the longer the program runs. It is much, much harder for the JVM
			to do memory optimization.
		3. You'll notice the first run of the whole program is significantly slower than the rest of the runs as well;
			this is because the JVM is still loading up some of its code in the background. You can switch which method
			runs first and which runs second and you will still see this slowdown at the very beginning.
 */
public class ArrayReversal {

    // This is the size of the arrays that will be reversed. You can edit this to see how the times and memory usage
    // changes. NOTE: If it is too big you'll get an OutOfMemory exception and your program will exit.
    private static int ARRAY_SIZE = 100000000;

    public static void main(String[] args) {
        System.out.println("\nExecuting with array of size " + ARRAY_SIZE);

        System.out.println("\n\nNew Array\n");

        for (int i = 0; i < 10; i++) {
            // Take the time before execution
            long startTime = currentTimeMillis();

            // The prints below emphasize that you are creating a whole new array here. While this method is definitely
            // less efficient, if you needed to still use the old, unreversed array then this method may still be worth
            // doing.
            int[] toReverse = generateArray();
            // System.out.println(toReverse[0]);
            int[] reversed = reverseArrayNew(toReverse);
            //System.out.println(toReverse[0]);
            //System.out.println(reversed[0]);

            printTimeElapsed(startTime);
        }

        System.out.println("\n\nIn Place\n");

        for (int i = 0; i < 10; i++) {
            long startTime = currentTimeMillis();

            // The prints below emphasize that this is reversing the same array. While this method is more efficient
            // you could no longer have access to the unreversed version.
            int[] arr = generateArray();
            // System.out.println(arr[0]);
            reverseArrayInPlace(arr);
            // System.out.println(arr[0]);

            printTimeElapsed(startTime);
        }
    }

    private static int[] reverseArrayNew(int[] array) {
        printMemoryUsage();
        int[] array2 = new int[array.length];

        for (int i = 0; i < array2.length; i++) {
            array2[i] = array[array.length - 1 - i];
        }
        printMemoryUsage();

        return array2;
    }

    private static void reverseArrayInPlace(int[] arr) {
        printMemoryUsage();
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        }
        printMemoryUsage();
    }

    // Helper method to create a fresh array
    private static int[] generateArray() {
        int[] arr = new int[ARRAY_SIZE];

        // Fill each item in the array with its index
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }

        return arr;
    }

    // Helper method to print out the time elapsed since the start time
    private static void printTimeElapsed(long startTime) {
        // Take the time after the execution
        long endTime = currentTimeMillis();
        // Subtract to get the total time elapsed
        long elapsedMillis = endTime - startTime;
        System.out.println(elapsedMillis + " milliseconds\n");
    }

    // Helper method to print out the current memory usage of the program
    private static void printMemoryUsage() {
        Runtime runtime = Runtime.getRuntime();

        // Subtract the free memory available from the total memory available to get the number of bytes used.
        long usedBytes = runtime.totalMemory() - runtime.freeMemory();

        // Convert to megabytes (there are 1024 bytes in a kilobyte and 1024 kilobytes in a megabyte)
        long usedMegabytes = usedBytes / 1024 / 1024;
        System.out.println(usedMegabytes + " megabytes");
    }

}
