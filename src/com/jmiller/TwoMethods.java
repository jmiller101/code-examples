package com.jmiller;

import java.util.Scanner;

public class TwoMethods {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int radius;

        System.out.println("Please enter the radius: ");
        radius = input.nextInt();

        double StaticArea = area(radius);

        System.out.println("The area is: " + StaticArea);


    }

    public static double area(int hello) {
        return hello * hello * Math.PI;
    }
}
