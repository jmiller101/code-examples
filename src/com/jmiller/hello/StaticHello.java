package com.jmiller.hello;

public class StaticHello {
    private static String HELLO_STRING = "Hello, world!";

    private String helloString = "Hello, world!";

    public static void hello() {
        // Can access HELLO_STRING because it is a static field
        System.out.println(HELLO_STRING);

        // Compiler error, cannot access non-static fields or variables from a static context. Just like the static main
        // method, we could need to instantiate this class in order to access its non-static field helloString.
        // System.out.println(helloString);
    }
}
