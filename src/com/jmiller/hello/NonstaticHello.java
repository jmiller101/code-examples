package com.jmiller.hello;

public class NonstaticHello {
    private static String HELLO = "Hello, ";

    private String helloString;

    // This is the constructor for this class. It accepts a single String as an argument.
    public NonstaticHello(String helloArg) {
        // Here, I assign the argument to the instance variable, helloString
        helloString = helloArg;
    }

    public void hello() {
        // This is not a compiler error! It's totally fine to access static methods and fields from non-static contexts.
        // Because this method cannot be accessed until the class is instantiated, we know that helloString has also
        // been initialized in the constructor above.
        String fullString = HELLO + helloString;
        System.out.println(fullString);
    }
}
