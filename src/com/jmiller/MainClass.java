package com.jmiller;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the radius: ");
        int radius = input.nextInt();

        AreaClass.calculate(radius);

    }
}
