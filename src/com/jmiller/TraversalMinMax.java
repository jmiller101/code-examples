import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Math;

public class TraversalMinMax{
	public static void main(String[] args) {
//array
		int[]x = {1,5,6,9,10,2,24,51,5351,3,-3,4,-2432};

//set counters for minimum values and maximum values to the first element in array
		int max = x[0];
		int min = x[0];

// For loop to iterate through every element in the array
		for (int i = 0; i < x.length; i++){
// if the element located at i is greater than the maxmimum value...
			if (x[i] > max) {
				max = x[i];
			}
// if the element located at i is greater than the minimum value...
			if (x[i] < min){
				min = x[i];
			}

// basically playing tag to see the max and min values.. everytime a new min/max is found, the if statement runs and attaches it to min/max. 
// for loop runs this game throughout all the elements in the array and max and min are established.
		}

System.out.println("The Maximum value in this array is: " + max);
System.out.println("The Minimum value in this array is: " + min);


}



// SAMPLE AP QUESTION

public boolean isDifficult(){
	
int trail[] = {100,150,105,120,90,80,50,75,75,70,80,90,100};
// we have an array. We need to calculate the difference between element 2 and element 1. If any of the elements ≥ to |30|, add to counter
// if the counter is ≥ 3, we need to return True, else return false

int counter = 0;


// we need to traverse thru all the data..

for (int i = 0; i<trail.length-1; i++) {
	// IMPORTANT: gotta put trail length -1 in for loop since we might go out of bounds towards the end. Unsure if necessary though but if i=12,
	// trail[i+1] will be out of bounds so i put -1 just in case.

	//TESTING
	System.out.print(trail[i+1] - trail[i] + " ");

	// next we need to calculate the difference between trail[i+1]-trail[i]
	// if ≥|30|, add to counter
	// how to do absolute value? java lang math .abs



	if (Math.abs(trail[i+1] - trail[i]) >= 30){
		counter++;
	}
}


if (counter >= 3){
	System.out.println("Trail is difficult");
	return true;

}else{
	System.out.println("Trail is not difficult");
	return false;

}




}
}


