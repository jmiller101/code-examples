# code-examples

Main.java takes in a given radius and calculates the area in ONE class and ONE method.
TwoMethods.java takes in a given radius (from Main Method) and calculates the area (in Area Method) in ONE class and TWO methods.

MainClass.java takes in a given radius and passes it to AreaClass.java
AreaClass.java calculates the area from the given radius.
The area is calculated in TWO Classes and TWO Methods.
